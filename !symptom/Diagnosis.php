<? 	session_start();  ?>

<html>
<head>
<link href="../styles/Diagnosis-style.css" rel="stylesheet" type="text/css"/>
<meta charset="utf-8">
</head>
<body>

<!-- 홈페이지 로고-->
<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>

<img src="../images/medi1.jpg" style="width:100%; height:95%">

<!-- 회원가입, 로그인 바로가기 -->
<p id="welcome">  <?	echo $_SESSION['user_id']."님 환영합니다!!"; ?>
	<p id="si_button"><a href="../!my/Mypage.php" class="asc">내 정보</a></p>
	<p id="login_button"><a href="../!loginout/Logout.php" class="asc">로그아웃</a></p>

<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>

<!-- 증상, 질병 선택하는 부분 -->
<span id="sym_button"><a href="sysmptom-choice1.php">증상으로 찾기</a></span>
<span id="sick_name_button"><a href="sickness.php">질병 검색하기</a></span>

</body>
</html>