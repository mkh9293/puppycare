<?session_start();?>
<html>
<head>
<meta charset="utf-8">
<link href="../styles/bootstrap.min.css" rel="stylesheet"/>
<script src="http://code.jquery.com/jquery.js"></script>
<script src="../jquerys/bootstrap.min.js"></script>
<script src="../jquerys/index.js"></script>
<link href="../styles/index.css" rel="stylesheet">


<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>

<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?></p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?} else {?>
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>
<?}?>
<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>

</head>
<body>
	<div class="container-fluid" id="imgs">
	<div class="row">
	<div class="span6 offset6"></div>
<div class="page-header" id="page-header">
 <h1>이럴때 당황마세요! <small>원하는 부위를 선택해보세요.</small></h1>
</div>
	<div class="container-fluid" id="cont">
	<div class="row-fluid">
		<div class="span4" id="s1" ><p id="p1">머리</p>
		<a href="../!symptom/tipselect.php?select=0"><img id="i1" src="../images/head3.png" class="img-rounded"></a>
		</div>
		<div class="span4" id="s2"><p id="p2">몸</p>
		<a href="../!symptom/tipselect.php?select=1"><img id="i2" src="../images/body.png" class="img-rounded"></a>
		</div>
		<div class="span4" id="s3" ><p id="p3">행동</p>
		<a href="../!symptom/tipselect.php?select=2"><img id="i3" src="../images/act2.png" class="img-rounded"></a>
		</div>


	<div class="span6 offset3" id="text-div">
		<form class="form-search" id="search_form" action="tipselect2.php" method="POST">
		<input type = "hidden" value="0" name="type">
		<input id = "search" name = "search" class="input-xxlarge" type="text" placeholder="검색어를 입력해주세요.">
		<button type="submit" class="btn btn-info btn-small" id="submit_btn"><i class="icon-search icon-white"></i></button>
	</form>
</div>
	</div>
	
</div>
</div>
</div>
</body>
</html>