﻿<?php @session_start();?>
<html>
<link href="../styles/symptom-choice-style.css" rel="stylesheet" type="text/css"/>
<script src="../jquerys/jquery-1.11.2.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf8">
<script src="../jquerys/symptom-jquery.js" type="text/javascript"></script>

<body>

<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>

<!-- <img id="main_img" margin="0" padding="0" src="../images/sym.jpg" width="100%" height="90%"> -->

<!-- 회원가입, 로그인 바로가기-->
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?> </p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>

<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>


<div id="step1_div">
<font size=5><strong>STEP 1</strong>. 부위를 선택하세요.</font>
</div>
<div id="ul_div">

<ul>
	<li class="title">머리
	<ul class=sub>
		<li id="li1">눈</li>
		<li id="li2">코</li>
		<li id="li3">입</li>
		<li id="li4">귀</li>
	</ul></li>

	<li class="title">몸
	<ul class=sub>
	<li id="li5">피부, 피모</li>
	<li id="li6">항문, 질</li>
	<li id="li7">만지는 것</li>
	<li id="li8">체중/체온</li>
	<li id="li9">배(위&장)</li>
	</ul></li>

	<li class="title">몸짓, 행동
	<ul class=sub>
	<li id="li10">호흡</li>
	<li id="li11">식욕&음식물</li>
	<li id="li12">목소리</li>
	<li id="li13">걷는 방법&기타 행동</li>
	</ul></li>
</ul>
</div>


<div id="step2_div">
<font size=5><strong>STEP 2</strong>. 강아지의 상태를 체크해보세요.</font>
</div>


<div id="list_div">
</div>

<form action="test.php" method="post" onSubmit="return Insert();" name="test_form">
<div id="pop_div" >
		<div id="pop_sub_div" style="border:0.5px solid; width:550px; height:270px; overflow:auto;">
		<table id="pop_table" >
			<tr class="main_tr">
				<td width=12%>부위</td><td width=73%>증상</td><td width=17%>지우기</td>
			</tr>
		</table>
	</div>
	<div id="pop_char" style="width:350px; text-align:center;">
	<font><?= $_SESSION['user_id']?>님이 선택한 강아지 증상</font> <input id="i1" type="submit" value="결과보기" >
</div>
</div>
	</form>

</body>
</html>