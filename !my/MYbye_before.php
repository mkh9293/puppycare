<? 	session_start();  ?>

<html>
<link href="../styles/MYchange-style.css" rel="stylesheet" type="text/css"/>
<head>
<meta charset="utf-8">
<script>
    function submitForm() {
    	if (confirm("정말 탈퇴하시겠습니까??") == true){    //확인
    		return true;
		}else{   //취소
   			 return false;
		}
	}
</script>
</head>
<body>

<div id="left_position">
	<ul class="navi"> 
		<li><a href="Mypage.php">내 글 목록</a></li>  
		<li><a href="MYboard_RE.php">내 댓글 목록</a></li> 
		<li><a href="MYchange.php">정보 수정</a></li> 
 		<li id="bye"><a href="MYbye_before.php">회원 탈퇴</a></li> 
	</ul> 
</div>

<table id="location">
<form action="MYbye_idpass.php" method="POST" onsubmit="return submitForm()">
<tr>
<td>아이디 </td>
<td align="center">
<input type=text placeholder="아이디" style="width:200px; height:30px;" name="insert_id">
</td>
</tr>
<tr>
<td>비밀번호 </td>
<td align="center">
<input type=password placeholder="비밀번호" style="width:200px; height:30px;" name="insert_pass">
</td>
</tr>
<tr>
<td align="right" colspan ="2">
<input type="submit" id = "submit_div" value="확인" >
</td>
</tr>
</form>
</table>


<!-- 홈페이지 로고 부분-->
<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>


<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?> </p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?}?>

<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>


</body>
</html>