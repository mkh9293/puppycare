<?session_start();
?>
<html>
<link href="../styles/freeboard-style.css" rel="stylesheet" type="text/css">
<link href="../styles/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="../jquerys/bootstrap.min.js"></script>
<meta charset="utf-8">
<body>

<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>

<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?></p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?} else {?>
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>
<?}?>

<!-- 게시판 목록 부분-->
 <div id="left_position">
 <ul class="navi" id = "navi"> 
    <li id="free"><a href="../!board/freeboard_list.php" >자유게시판</a></li> 
    <li><a href="../!board/Proud-right-frame.php">자랑하기</a></li> 
    <li><a href="../!board/shareboard_list.php">증상공유</a></li> 
  </ul>
 </div>

 <script>
  function submitForm() {
    if (confirm("정말 삭제하시겠습니까??") == true){    //확인
        true;
    }else{   //취소
         return false;
    }
  }
</script>

<?php

  function edit_restTime($write_time){
      $diff = time() - strtotime($write_time); 
      if ( $diff>=1 && ($diff/3600 < 1)) { 
        $rest_time =  ceil($diff/60).'분 전'; 
        return $rest_time;
      } 
      else if ($diff==0) {
        return "방금";
      }
      else {
        $time_split0 = explode(" ", $write_time);
        $time_split = explode("-", $time_split0[0]);
        $rest_time = $time_split[0].'.'.$time_split[1].'.'.$time_split[2];
        return $rest_time;
      }//올린시간이 한시간 이전이면 몇분전으로 나오고 아니면 년/월/일로 나온다.

  }

  include_once('dbcon.php');
  mysql_query("set names 'utf8'");

  $_SESSION["board_num"] =  $_GET['board_no'];

  $result = mysql_query("select * from free_board where board_no='".$_SESSION["board_num"]."'", $conn);
  $row = mysql_fetch_array($result);  //게시글 불러오기
 
  $comment_list = mysql_query("select * from  fb_comment where board_num='".$_SESSION["board_num"]."'", $conn);

  $comment_no = mysql_query("select Count(comment_no) from fb_comment where board_num='".$_SESSION["board_num"]."'", $conn);
  $com_num=mysql_fetch_array($comment_no);

  $hit_update = mysql_query("update free_board set hit = hit + 1 where board_no='".$_SESSION["board_num"]."'", $conn);

  $hit_no = mysql_query("select hit from free_board where board_no='".$_SESSION["board_num"]."'", $conn);
  $hit_num=mysql_fetch_array($hit_no);
?>

<!-- 게시판 전체 -->
<div class="container" id="show-div">

  <!-- 게시판 수정,삭제,목록 버튼-->
  <div class="row-fluid" >

    <ul class="nav nav-pills" id="head-content">
<?if(!strcmp($_SESSION[user_id], $row[writer])) { ?>
   <li><form action = "frb_update_page.php" method = "POST">
    <input type = "hidden" value = <?echo$row['board_no']?> name = "board_num">
     <button type = "submit" class="btn btn-info btn-small"><i class="icon-wrench icon-white"></i> 수정</button>
   </form></li>
   <li><form action = "frb_delete.php" method = "POST">
    <input type = "hidden" value = <?echo$row['board_no']?> name = "board_num">
    <button type = "submit" onclick="return confirm('삭제하시겠습니까?')" class="btn btn-info btn-small"><i class="icon-remove icon-white"></i> 삭제</button>
   </form> </li>  
   <li ><button onclick="javascript:location.href='../!board/freeboard_list.php'" class="btn btn-info btn-small"><i class="icon-list icon-white"></i> 목록</button></li>
   </ul>
 <? }else{?>
  <ul class="nav nav-pills" id="head-content">
   <li ><button onclick="javascript:location.href='../!board/freeboard_list.php'" class="btn btn-info btn-small"><i class="icon-list icon-white"></i> 목록</button></li>
    </ul>
<? }  ?>

  <!-- 게시판 제목 부분-->
  <div class="row-fluid">
    <div class="span12">
    <table class="table" id="board-show">
      <colgroup>
              <col width="8%">
              <col width="45%">
              <col width="*">
      </colgroup>
      <tr>
        <thaed>
          <th>제목<span>|</span></th>
          <th><? echo $row['board_name']; ?></th>
          <th></th><th></th><th></th><th></th><th></th><th></th>
        </thaed>
      </tr>
      <tr>
        <td></td><td></td>
        <td>글쓴이</td><td><strong><? echo $row['writer']; ?></strong></td>
        <td>등록일</td><td><strong><? echo edit_restTime($row['write_time']); ?></strong></td>
        <td>조회수</td><td><strong><?echo $hit_num[0];?></strong></td>
      </tr>
    </table>
</div>
</div>

<!-- 글 내용 부분-->
<div class="row-fluid" id="content-div">
  <div class="span12">
  <? echo $row['board_content']; ?>
</div>
</div>

<!-- 게시판 조회수, 댓글수 부분-->

<div class="row-fluid">
  <div class="span12">
  <strong>댓글 <?echo $com_num[0];?>개</strong>
  <strong>조회수 <?echo $hit_num[0];?>개</strong>
</div>
</div>

<!-- 게시판 댓글달기 부분-->
<? if ($_SESSION['user_name']) {?>
<div class="row-fluid" id="comment-text-div">
  <div class="span12">

  <form  action="comment_write.php" method="post">
    <div class="input-append">
    <input type = "text" name = "comment" placeholder="댓글을 남겨주세요." class="input-xxlarge" id="comment-text">
    <button type = "submit" class="btn" >등록</button>   
    <input type = "hidden" name = "$board_num" value=<?echo $board_num?> >
  </div>    
</form>
</div>
</div>
<? } ?>

<div class="row-fluid" id="comment-show-div">
<div class="span12">
  <?
  if($result){
        while($rs=mysql_fetch_array($comment_list)) {
          $com_date = edit_restTime($rs[c_date]);
          ?>
  

<!-- 댓글 세부내용(작성자면 삭제버튼 보이고 아니면 댓글만보이기) 부분-->
      <form action="com_delete.php" method="post" onsubmit="return submitForm()" id="comment-show-form">
        <strong><? echo $rs[comment_writer]; ?> </strong> 
        <? echo $com_date; ?>
      <? if(!strcmp($_SESSION['user_id'], $rs[comment_writer])) { ?>
        <input name = "board_no" type="hidden" value=<? $_SESSION["board_num"] ?>>
        <input name = "comment_no" type="hidden" value=<? echo $rs[comment_no]; ?>>
      
        <input  TYPE="image" src="../images/ex.png" name="submit" value="submit"  style="width:15px; height:15px;">    
         
        <?}?>
        <p>
       <? echo $rs[comment_content]; ?>
     </p> 
 </form>
    <?}
  }?>

</div>
</div>
</div>
</body>
</html>

