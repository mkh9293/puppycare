<?session_start();?>

<html>
<meta charset="utf-8">
<link href="../styles/freeboard-style.css" rel="stylesheet" type="text/css">
<link href="../styles/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="../jquerys/bootstrap.min.js"></script>
<script>
	function write_chk(){
		<?
		if($_SESSION['user_name']) {?>
			self.location='freeboard_write.php';
		<?}
		else {?>
			alert('로그인 해주세요');
			self.location='../!loginout/Login.html';
		<?}?>
	}

</script>
<body>

<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>

<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>

<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?></p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?} else {?>
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>
<?}?>

<!-- 게시판 목록 부분-->
<div id="left_position">
 <ul class="navi" id = "navi"> 
		<li id="free" class="free"><a href="../!board/freeboard_list.php">자유게시판</a></li> 
		<li id="proud" class="proud"><a href="../!board/Proud-right-frame.php">자랑하기</a></li> 
		<li><a href="../!board/shareboard_list.php">증상공유</a></li> 
  </ul>
 </div>

<?

	function edit_restTime($write_time){
			$diff = time() - strtotime($write_time); 
			if ( $diff>=1 && ($diff/3600 < 1)) { 
				$rest_time =  ceil($diff/60).'분 전'; 
				return $rest_time;
			} 
			else {
			 	$time_split0 = explode(" ", $write_time);
				$time_split = explode("-", $time_split0[0]);
				$rest_time = $time_split[0].'년'.$time_split[1].'월'.$time_split[2].'일';
				return $rest_time;
			}//올린시간이 한시간 이전이면 몇분전으로 나오고 아니면 년/월/일로 나온다.

	}


	include_once('dbcon.php');
	mysql_query("set names 'utf8'");
	$pagenum=$_GET['pagenum'];
	$src_value=$_POST['src_value'];
	$src_name=$_POST['src_name'];
	if($pagenum == "") $pagenum=0;

	if($src_value!=''){
		$where=" where $src_name like '%$src_value%'";
	}
	$query="select count(*) from free_board";
	$result = mysql_query($query)or die(mysql_error());
	$row=mysql_fetch_array($result);
	$total = $row[0]; 

	$page = 8; 
	$pagesu = ceil ($total/$page); 
	$start = $page * $pagenum; 
	 
	$query = "select * from free_board $where order by board_no DESC limit $start, $page";
	$result = mysql_query ($query, $conn);
	$pagegroup=5; 
	$pageend = $pagestart + $pagegroup; 
	$pagegroupnum=ceil(($pagenum+1)/$pagegroup); 
	$pagestart=($pagegroupnum -1)*$pagegroup +1; 
	$pageend = $pagestart + $pagegroup-1; 

	$time_str = "";
?>
 	
<!-- 게시판 전체 묶음-->
<div class="container" id="board-list-div">

	<div class="row-fluid">
<!-- 게시판 테이블-->
	<div class="span12">
	<table class="table table-hover" id="board-list">
		<fieldset>
			<legend>자유게시판 입니다.</legend>
		<colgroup>
			<col width="10%">
			<col width="*">
			<col width="15%">
			<col width="18%">
			<col width="9%">
		</colgroup>

		<thead>
		<tr>
			<th >번호</th>
			<th >제목</th>
			<th>작성자</th>
			<th >등록일</th>
			<th >조회수</th>
		</tr>
	</thead>
		<?
			$no = $total - ($pagenum * $page);
			if($result){
				while($rs=mysql_fetch_array($result)) {
		

					$time_str = edit_restTime($rs[write_time]);
		?>
		<tr>
			<td ><strong><? echo $no;?><strong></td>
			<td> <a href="freeboard_show.php?board_no=<? echo $rs[board_no];?>;"><? echo $rs[board_name]; ?></a></td>
			<td ><? echo $rs[writer];?></td>
			<td ><? echo $time_str?></td>
			<td ><?echo number_format($rs[hit]);?></td>
		</tr>

		<?
			$no--;
			}
		}
		?>
	</fieldset>
	</table>
</div>
</div>

<!--페이지 이동 -->
<div class="pagination pagination-small pagination-centered" id="pagination-div">
			<?
			for($i=$pagestart; $i<=$pageend; $i++)
			{ //페이지 이동 버튼 출력
				  if($i>$pagesu){ break;}
					  $j=$i-1; //넘겨줄 $pagenum
				  if($pagenum==$j){
				  echo "<ul><li class='disabled'><a href='$PHP_SELF?pagenum=$j' >$i</a></li></ul>";
				}else{
					 echo "<ul><li><a href='$PHP_SELF?pagenum=$j' >$i</a></li></ul>";
				}
			}
			?>
</div>

<!-- 페이지 검색-->
<div class="input-prepend" id="search-div">			
			<form class="form-search" action='freeboard_list.php' method="post" name="search_form" id="search_form">
			
							<input name="page" type="hidden" id="page" value=<?echo "$page";?>>
							<select name="src_name" id="search-select">
								<option value="board_name" selected>제목</option>
								<option value="board_content">내용</option>
								<option value="writer">아이디</option>
							</select>
					
							<input name="src_value" type="text" id="search-text">
					
							<button type="submit" class="btn" name="submit_btn" id="submit_btn" >검색</button>
							
				</form>

</div>
<div id="btn-div">
		<button id="wr_btn" class="btn" onclick="write_chk()">글쓰기</button>
</div>
</body>
</html>