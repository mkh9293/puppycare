﻿<?session_start();?>
<html>
<link href="../styles/freeboard-style.css" rel="stylesheet" type="text/css">
<link href="../styles/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="../jquerys/bootstrap.min.js"></script>
<script type="text/javascript" src="../se2/js/HuskyEZCreator.js" charset="utf-8"></script> 
<meta charset="utf-8">
<script>
$(document).ready(function(){

	
$("#submit_btn").click(function() {

    oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);

    var text=document.getElementById("tname");
    if(text.value.trim()==''){
    	alert("제목을 입력해주세요.");
    	text.focus();
    	return false;
    }
    else{
        submit();
    }
});

$("#reset_btn").click(function(){

	var title=document.getElementById("tname");
	title.value='';
	oEditors.getById["ir1"].exec("SET_IR", [""]);
	return false;
});

});
</script>
<body>

<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>
<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?></p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?} else {?>
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>
<?}?>

<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_id']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<!-- <td><a href="../!hospital/hospital.html" class="desc"><p>병원 위치</p></a></td> -->
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<!-- <td><a href="../!hospital/hospital.html" class="desc"><p>병원 위치</p></a></td> -->
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>

<div id="left_position">
 <ul class="navi" id = "navi"> 
		<li id="free" class="free"><a href="freeboard_list.php">자유게시판</a></li> 
		<li ><a href="Proud-right-frame.php">자랑하기</a></li> 
		<li ><a href="shareboard_list.php">증상공유</a></li> 
  </ul>
 </div>

 <!-- 게시판 글쓰기 부분-->
 <div class="container" id="board-write-div">
 	<div class="row-fluid">
	<form action="freeboard_write_check.php" method="post">
		<fieldset>
			<legend>자유게시판 글쓰기 입니다.</legend>
			<input type="text" id="tname" name="title" class="input-block-level" placeholder="제목을 입력해주세요.">
			
			<textarea name="ir1" id="ir1" rows="18" class="span12"></textarea>

		<div class="row-fluid" id="row2">
			<button id ="submit_btn" class="btn btn-success"><i class="icon-ok icon-white"></i> 확인</button>
			<button id ="reset_btn" class="btn btn-warning" ><i class="icon-remove icon-white"></i> 취소</button>
	</div>
</fieldset>
</form>

</div>
</div>

<script>
var oEditors = [];
nhn.husky.EZCreator.createInIFrame({

    oAppRef: oEditors,
    elPlaceHolder: "ir1",
    sSkinURI: "../se2/SmartEditor2Skin.html",
    fCreator: "createSEditor2"
});

</script>
</body>
</html>
