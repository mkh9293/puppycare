<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../styles/Signup-style.css" rel="stylesheet" type="text/css"/>
<script src="../jquerys/jquery-1.11.2.js" type="text/javascript"></script>
<script>

	function checkForm(thisform){
	
		if(thisform.ch_id.value==0){
			alert("id 중복체크를 해주세요.");
			thisform.id.focus();
			return false;
		}
		else if(thisform.id.value.trim()==''){
			alert("아이디를 입력해주세요.");
			thisform.id.focus();
			return false;
		}
		else if(thisform.pass.value.trim()==''){
			alert("비밀번호를 입력해주세요.");
			thisform.pass.focus();
			return false;
		}
		else if(thisform.pass.value.length<8){
			alert("비밀번호를 8자 이상으로 작성해주세요.");
			thisform.pass.focus();
			return false;
		}
		else if(thisform.pass2.value.trim()==''){
			alert("비밀번호 확인을 입력해주세요.");
			thisform.pass2.focus();
			return false;
		}
		else if(thisform.pass.value!=thisform.pass2.value){
			alert("비밀번호를 잘못입력하셨습니다.");
			thisform.pass.value="";
			thisform.pass2.value="";
			thisform.pass.focus();
			return false;
		}
		else if(thisform.user_name.value.trim()==''){
			alert("이름을 입력해주세요.");
			thisform.user_name.focus();
			return false;
		}
		else if(thisform.mail.value.trim()==''){
			alert("메일을 입력해주세요.");
			thisform.mail.focus();
			return false;
		}
		else if(thisform.code.value!=thisform.codeval.value){
			alert("인증메일이 잘못되었습니다.");
			thisform.code.focus();
			return false;
		}
		else{
			return true;
		}

	}

	// 아이디 중복 여부 확인
	function checkid(thisform){

		if($('#idx').val().length<='10'){
			var len=$('#idx').val();
			$.ajax({
				url:'check_id.php',
				type:'post',
				data:'id='+len,
				success:function(data){
					$('#s1').text('').text(data);
					if(data[2]=='사'){
						document.getElementById('ch_id').value=1;
					}else{
						document.getElementById('ch_id').value=0;
					}
				}
			});
		}else{
			$('#s1').css({'color':'red','font-weight':'bold'});

		}
	}


	$(document).ready(function(){

	$('#idx').keyup(function(){
		var ch=document.getElementById('ch_id');
		ch.value=0;
		if($('#idx').val().length>'10'){
			$('#s1').text('').text('* 아이디를 10자 이하로 해주세요.').css({'color':'red','font-weight':'bold'});
		}
		else{
			$('#s1').css({'color':'green','font-weight':'normal'});
		}
	});

	$('#pass').keyup(function(){
		if($('#pass').val().length<='8'){
			$('#s2').text('').text('* 비밀번호를 8자 이상으로 작성해주세요.').css({'color':'red'});
		}
		else{
			$('#s2').text('').text(' * 사용가능한 비밀번호 입니다.').css({'color':'green','font-weight':'normal'});
		}
	});
});
	
	function mailto(thisform){
		var left=Math.ceil((window.screen.width-300)/2);
		var top=Math.ceil((window.screen.height-300)/2);
		var checkId=window.open('mail.php?id='+thisform.mail.value,'CheckWindow','top='+top+',left='+left+',width=180, height=100,toolbar=no,status=no,resizable=no');
			checkId.focus();

	}
</script>

<body>


<!-- 회원가입 폼-->
<form action="signup-check.php" method="POST" id="si_form" onSubmit="return checkForm(this)">
<div id="si_form_div">
<input type=text placeholder="아이디" style="width:300px; height:30px;" name="id" id="idx" onkeyup="checkid()" tabindex="1" > <span id="s1" style=" top:30px; color:red; font-size:11px;">* 아이디를 10자 이하로 작성해주세요.</span><br><br>
<input type="hidden" name="ch_id" id="ch_id" value="0">
<input type=password placeholder="비밀번호" style="width:300px; height:30px;" name="pass" id="pass" tabindex="2"><span id="s2" style="left:10px; top:50px; color:red; font-size:11px;"> * 비밀번호를 8자 이상으로 작성해주세요.</span><br><br>
<input type=password placeholder="비밀번호 확인" style="width:300px; height:30px;" name="pass2" id="pass2" tabindex="3"><Br><Br>
<input type=text placeholder="이름" style="width:300px; height:30px;" name="user_name" tabindex="4"><Br><Br>
<input type=text placeholder="이메일" style="width:300px; height:30px;" name="mail" tabindex="5"> &nbsp <input id = "mail_btn" type=button onclick=mailto(this.form) value="인증메일전송"><br><br>
<input type=hidden name='codeval' id='codeval' value='0'> 
<input type=text placeholder="메일인증코드" style="width:300px; height:30px;" name="code" tabindex="6"><br><br>
<input type=text placeholder="찾게된 계기" style="width:300px; height:30px;" name="how" tabindex="6"><br><br>
<textarea placeholder="자기소개"  style="width:300px; height:110px; resize:none;" name="introduce" tabindex="7"></textarea><br><br>
<input type="submit" value="확인" id="si_form_submit" style=" position:absolute; left:240px; width:60px; height:35px;">
</div>
</form>

<!-- 홈페이지 로고 부분-->
<div id="img_div"><a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a></div>

<!-- 회원가입 , 로그인 바로가기 버튼-->
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>

<!-- 홈페이지 밑부분-->
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>

</body>
</html>