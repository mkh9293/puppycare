$(document).ready(function(){
	var index=0;
	var width=700;
	var i=0;

	$("#img-ul2 li:nth-child(1)").css("cursor","pointer").click(function(){
		i=0;
		ind=i;
		show(ind);
		$("#img-ul2 li:not(:nth-child(1)").removeClass("select");
		$("h2:not(:nth-child(1)),h4:not(:nth-child(2))").addClass("text-select");
	});
	$("#img-ul2 li:nth-child(2)").css("cursor","pointer").click(function(){	
		i=2;
		ind=i;
		show(ind);
		$("#img-ul2 li:not(:nth-child(2)").removeClass("select");
		$("h2:not(:nth-child(5)),h4:not(:nth-child(6))").addClass("text-select");
	});
	$("#img-ul2 li:nth-child(3)").css("cursor","pointer").click(function(){	
		i=5;
		ind=i;
		show(ind);
		$("#img-ul2 li:not(:nth-child(3)").removeClass("select");
		$("h2:not(:nth-child(11)),h4:not(:nth-child(12))").addClass("text-select");
	});

	$("#aleft").css("cursor","pointer").click(function(){
		if(i>0){
			--i;
			var ind=index-1;
			show(ind);
		}
	});

	$("#aright").css("cursor","pointer").click(function(){
		if(i<7){
			++i;
			var ind=index+1;
			show(ind);
		}
		else if(i==7){
			i=0;
			ind=i;
			show(ind);
		}
	});

	function show(ind){
		var posi=-width*ind;
		$('#wrap').animate({left:posi},300,function(){
			switch(posi){
				case -700:
				$("h2:nth-child(5),h4:nth-child(6)").addClass("text-select");
				$("#img-ul2 li:nth-child(2)").removeClass("select");
				$("#img-ul2 li:nth-child(1)").addClass("select");
				$("h2:nth-child(1),h4:nth-child(2)").addClass("text-select");
				$("h2:nth-child(3),h4:nth-child(4)").removeClass("text-select");
				break;
				case -1400:
				$("h2:nth-child(7),h4:nth-child(8)").addClass("text-select");
				$("h2:nth-child(3),h4:nth-child(4)").addClass("text-select");
				$("h2:nth-child(5),h4:nth-child(6)").removeClass("text-select");
				$("#img-ul2 li:nth-child(1)").removeClass("select");
				$("#img-ul2 li:nth-child(2)").addClass("select");
				break;
				case -2100:
				$("h2:nth-child(9),h4:nth-child(10)").addClass("text-select");
				$("h2:nth-child(5),h4:nth-child(6)").addClass("text-select");
				$("h2:nth-child(7),h4:nth-child(8)").removeClass("text-select");
				break;
				case -2800:
				$("#img-ul2 li:nth-child(3)").removeClass("select");
				$("#img-ul2 li:nth-child(2)").addClass("select");
				$("h2:nth-child(11),h4:nth-child(12)").addClass("text-select");
				$("h2:nth-child(7),h4:nth-child(8)").addClass("text-select");
				$("h2:nth-child(9),h4:nth-child(10)").removeClass("text-select");
				break;
				case -3500:
				$("h2:nth-child(13),h4:nth-child(14)").addClass("text-select");	
				$("h2:nth-child(9),h4:nth-child(10)").addClass("text-select");
				$("h2:nth-child(11),h4:nth-child(12)").removeClass("text-select");
				$("#img-ul2 li:nth-child(2)").removeClass("select");
				$("#img-ul2 li:nth-child(3)").addClass("select");
				break;
				case -4200:
				$("h2:nth-child(15),h4:nth-child(16)").addClass("text-select");
				$("h2:nth-child(11),h4:nth-child(12)").addClass("text-select");
				$("h2:nth-child(13),h4:nth-child(14)").removeClass("text-select");
				break;
				case -4900:
				$("h2:nth-child(13),h4:nth-child(14)").addClass("text-select");
				$("h2:nth-child(15),h4:nth-child(16)").removeClass("text-select");
				break;
				default:$("h2:nth-child(15),h4:nth-child(16)").addClass("text-select");
				$("h2:nth-child(3),h4:nth-child(4)").addClass("text-select");
				$("h2:nth-child(1),h4:nth-child(2)").removeClass("text-select");
				$("#img-ul2 li:nth-child(3)").removeClass("select");
				$("#img-ul2 li:nth-child(1)").addClass("select");
			}
		});
		index=ind;
	}
	var logo=$("#si_button").offset();
	$("#top_logo").click(function(){
		$('body','html').animate({scrollTop:logo.top},500);
	});
});