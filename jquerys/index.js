$(document).ready(function(){
	
	$('img').hover(function(){
		$(this).css({"cursor":"pointer"});
		var point=$(this).attr("id");
		switch(point){
			case "i1":$("#p1").css({"display":"block"});
				  $("#s1").css("opacity","0.5");
				  break;
			case "i2":$("#p2").css({"display":"block"});
				  $("#s2").css("opacity","0.5");
				  break;
			default :$("#p3").css({"display":"block"});
				 $("#s3").css("opacity","0.5");
		}
	},function(){
		$(this).css({"opacity":"1"});
		var point=$(this).attr("id");
		switch(point){
			case "i1":$("#p1").css({"display":"none"});
			$("#s1").css("opacity","1");
			break;
			case "i2":$("#p2").css({"display":"none"});
			$("#s2").css("opacity","1");
			break;
			default :$("#p3").css({"display":"none"});
			$("#s3").css("opacity","1");
		}
});
});
