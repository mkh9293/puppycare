<?php session_start();  ?>
<html>
<link href="../styles/Inquire-style.css" rel="stylesheet" type="text/css"/>
<link href="../styles/bootstrap.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="../jquerys/bootstrap.min.js"></script>
<head>
<meta charset="utf-8">
</head>
<body>


<img id="main_img" src="../images/question.jpg" style="width:100%; ">

<!-- 홈페이지 로고-->
<a href="../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>


<!-- 회원가입, 로그인 바로가기 -->
<? if ($_SESSION['user_name']) {?>
<p id="welcome">  <?	echo $_SESSION['user_id']."님 환영합니다!!"; ?>
<p id="si_button"><a href="../!my/Mypage.php" class="asc">내 정보</a></p>
<p id="login_button"><a href="../!loginout/Logout.php" class="asc">로그아웃</a></p>

<?} else{?>
<p id="si_button"><a href="../!signup/Signup.php" class="asc">회원가입</a></p>
<p id="login_button"><a href="../!loginout/Login.html" class="asc">로그인</a></p>
<?}?>


<!-- 문의하기 폼 -->
<div class="container-fluid" id="cont-div">
	<div class="row-fluid">
		<div class="span12">
			<form class="form-horizontal" action="Inquire.php" method="POST">
				<fieldset>
					<legend>문의하기</legend>
						<div class="form-group">
							<label for="subject">제목</label>
							<input class="span12" type="text" placeholder="제목을 입력해주세요." id="subject">
						</div>
						<Br/>
						<div class="form-group">
							<label for="email">메일</label>
							<input class="span12" type="text" placeholder="메일을 입력해주세요." id="email">
						</div>
						<Br/>
					<div class="form-group">
      					<label for="comment">내용</label>
      					<textarea class="form-control" rows="10" id="comment" style="width:100%"></textarea>
    				</div>
    				<Br/>
				</fieldset>
				<button type="submit" class="btn">보내기</button>
			</form>
		</div>
	</div>
</div>


<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
</table>
</div>
<?}?>





</body>
</html>