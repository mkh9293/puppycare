<?php @session_start();?>
<html>
<link href="../styles/Intro-style.css" rel="stylesheet" type="text/css"/>
<script src="../jquerys/jquery-1.11.2.js" type="text/javascript"></script>
<script src="../jquerys/jquery2.js" type="text/javascript"></script>
<head>
<meta charset="utf-8">
</head>
<body>

<!-- 홈페이지 로고-->
<a href="../!main/../!main/Main.php"><img id="main_logo" src="../images/logo.png"></a>


<!-- 맨 위에 부분-->

<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_name']."님 환영합니다."; ?>
  <p id="si_button"><a id = "info" href="../!my/Mypage.php">내 정보</a></p>
  <p id="login_button"><a id = "logout" href="../!loginout/Logout.php">로그아웃</a></p>
<?} else {?>
  <p id="si_button"><a href="../!signup/Signup.php" class="asc">회원가입</a></p>
  <p id="login_button"><a href="../!loginout/Login.html" class="asc">로그인</a></p>
<?}?>


<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_id']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<!-- <td><a href="../!hospital/hospital.html" class="desc"><p>병원 위치</p></a></td> -->
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
<img id="top_logo" src="../images/up.png" style=" position:absolute; right:3.1%; margin-top:2; width:45px; height:50px; ">
<b style="position:absolute; right:5%; top:63%; width:10px; height:10px; "></b>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<!-- <td><a href="../!hospital/hospital.html" class="desc"><p>병원 위치</p></a></td> -->
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
<img id="top_logo" src="../images/up.png" style=" position:absolute; right:3.1%; margin-top:2; width:45px; height:50px; ">
<b style="position:absolute; right:5%; top:63%; width:10px; height:10px; "></b>
</table>
</div>
<?}?>




<!-- 메인 소개 부분 div 영역-->
<div id="main_intro_wrap_div">
<!-- <p id="char_one">동물은 몸짓으로 말을 합니다. 경청해주세요.</p>
 -->
<!-- 메인 소개 부분 div 영역-->
<div id="main_intro_div">
<table>
<td id="diag_intro_td"><img src="../images/medi4.png" widht="150" height="25%" id="i6">
<h2>진단 소개</h2>
<p>내 강아지의 상태만으로</p> 
<p>몸상태를 진단할 수 있어요</p>


<td id="location_intro_td"><img src="../images/person.png" widht="150" height="25%" id="i7">
<h2>이럴때 당황마세요</h2>
<p>내 강아지의 현상태는?</p>
<p>이 서비스는 준비중이에요</p>


<td><img src="../images/board4.png" widht="150" height="25%" id="i8">
<h2>게시판 소개</h2>
<p>애견인들과 소통하고</p>
<p>귀여운 내 강아지를 자랑해보세요</p></td>
</table>
</div>
</div>


<!-- 진단에 대한 소개 영역-->
<div id="diag_intro_wrap_div">
<img id="sub_img_slide_right" src="../images/arrow_r.png">
<img id="sub_img_slide_left" src="../images/arrow_l.png">
<img id="sub_img_slide_right2" src="../images/arrow_r.png">
<img id="sub_img_slide_left2" src="../images/arrow_l.png">

<div id="diag_intro_sub">
<div id="container">
<div id="slide_wrap">
<img id="sub_img" src="../images/sub1.jpg" style="float:left">
<img id="sub_img2" src="../images/sub2.jpg">
<img id="sub_img3" src="../images/sub3.jpg">
</div>
</div>
<p style="position:absolute; left:-60%; top:250px; font-size:18px; border:2px solid; padding:3px; font-weight:bolder">증상으로 찾기</p>
<p id="diag_p1">아픈 증상을 나타내는 부위를 선택하세요!</p>
</div>

<div id="diag_intro_sub2">
<img id="sub_img4" src="../images/search.jpg">
<p style="position:absolute; top:180px; left:-60%; font-size:18px;border:2px solid; padding:3px; font-weight:bolder">질병검색하기</p>
<p id="diag_p2">궁금한 질병명을 검색하세요!</p>
</div>

<div id="diag_intro_main">
<img id="main_img" src="../images/main1.png">
<p style="position:absolute; top:105%; left:11%; FONT-size:17px">증상으로 찾고, 질병명으로도 검색 가능해요!</p>
</div>

<img id="arrow1" src="../images/arrow1.png">

<img id="arrow2" src="../images/arrow2.png">
</div>




<!-- 게시판에 대한 소개 영역-->
<div id="board_intro_wrap_div">
<ul>

<li><img src="../images/free.jpg" style="width:400px; height:200px "><img src="../images/board4.png" style="width:100; height:100; position:absolute; margin-top:40px; margin-left:80px;">
<font style="position:relative; top:-90px; margin-left:180px; font-size:30px">자유게시판</font>
<font style="position:relative; top:-50px; margin-left:-200px; font-size:18px">자유롭게 글을 적어보세요.</font>
</li>

<li><img src="../images/share.jpg" style="width:400px; height:200px "><img src="../images/board4.png" style="width:100; height:100; position:absolute; margin-top:40px; margin-left:80px;">
<font size=5 style="position:relative; top:-90px; margin-left:180px;font-size:30px">증상공유 게시판</font>
<font size=4 style="position:relative; top:-50px; margin-left:-260px;font-size:18px">자신의 강아지가 겪었던 증상을 공유해보세요.</font>
</li>

<li><img src="../images/proud.jpg" style="width:400px; height:200px "><img src="../images/board4.png" style="width:100; height:100; position:absolute; margin-top:40px; margin-left:80px;">
<font size=5 style="position:relative; top:-90px; margin-left:180px;font-size:30px">자랑하기 게시판</font>
<font size=4 style="position:relative; top:-50px; margin-left:-235px;font-size:18px">자신의 강아지를 자랑해보세요.</font>
</li>

</ul>
</div>

<!-- 표-->
<div id="diag_navi_div">
<font style="position:absolute; margin-top:4%; margin-left:7%; font-size:35px; font-weight:bolder">Q. 진단은 어떻게 하나요?</font>
<p style="position:absolute; margin-left:7%; margin-top:8%; font-size:26px">A. 각 카테고리에 맞는 부위를 확인하세요!</p>
<table border=1px solid; style=" position:absolute; margin-top:13%; margin-left:5%; font-size:30px; text-align:center; width:90%; height:70%; border:1.5px solid gray; border-radius:10px;">

<tr>

<td rowspan=7 style="width:100px; box-shadow:2px 2px 2px 2px #999;"><font size=7>머리</font></td>

<td>눈</td>

<td rowspan=7 style="width:100px; box-shadow:2px 2px 2px 2px #999;"><font size=7>몸</font></td>

<td>피부/피모</td>

<td rowspan=7 style="width:100px; box-shadow:2px 2px 2px 2px #999;"><font size=7><p>행동</p><p>&</p><p>몸짓</p></font></td>

<td>호흡</td>

</tr>

<tr>
<td rowspan=2>코</td>
<td>발톱</td>
<td rowspan=2>식욕&음식물</td>
</tr>

<tr>
<td>항문/질</td>
</tr>

<tr>
<td rowspan=2>귀</td>
<td>만지는 것</td>
<td rowspan=2>목소리</td>
</tr>

<tr>
<td>체중/체온</td>
</tr>

<tr>
<td rowspan=2>입/이빨</td>
<td rowspan=2>배(위&장)</td>
<td rowspan=2>걷는 방법 & 기타 행동</td>
</tr>

</table>
</div>
</body>
</html>