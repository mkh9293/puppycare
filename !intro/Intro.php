﻿<?php @session_start();?>

<html>
<head>
<meta charset="utf-8">
<link href="../styles/bootstrap.min.css" rel="stylesheet"/>
<link href="../styles/Intro-style2.css" rel="stylesheet" type="text/css"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src="../jquerys/bootstrap.min.js"></script>
<script src="../jquerys/intro.js"></script>
</head>
<body>

	<!--홈페이지 로고 -->
<a href="../!main/../!main/Main.php"><img id="main_logo" src="../images/logo.png"/></a>


<!-- 맨 위에 부분-->
<? if ($_SESSION['user_name']) {?>
  <p id="welcome2">  <? echo $_SESSION['user_id']."님 환영합니다."; ?></p>
  <a id = "info" href="../!my/Mypage.php"><p id="si_button">내 정보</p></a>
  <a id = "logout" href="../!loginout/Logout.php"><p id="login_button">로그아웃</p></a>
<?} else {?>
  <a href="../!signup/Signup.php" ><p id="si_button">회원가입</p></a>
  <a href="../!loginout/Login.html" class="asc"><p id="login_button">로그인</p></a>
<?}?>


<!-- 홈페이지 밑부분-->
<? if ($_SESSION['user_name']) {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
<img id="top_logo" src="../images/up.png" style=" position:absolute; right:3.1%; margin-top:2; width:45px; height:50px; ">
<b style="position:absolute; right:5%; top:63%; width:10px; height:10px; "></b>
</table>
</div>
<?} else {?>
<div id="main_bottom_div">
<table>
<td><a href="../!intro/Intro.php" class="desc"><p>소개</p></a></td>
<td><a href="../!symptom/Diagnosis_NO.php" class="desc"><p>진단</p></a></td>
<td><a href="../!symptom/tip.php" class="desc"><p>애견지식</p></a></td>
<td><a href="../!board/freeboard_list.php" class="desc"><p>게시판</p></a></td>
<td><a href="../!inquire/Inquiremain.php" class="desc"><p>문의</p></a></td>
<img id="top_logo" src="../images/up.png" style=" position:absolute; right:3.1%; margin-top:2; width:45px; height:50px; ">
<b style="position:absolute; right:5%; top:63%; width:10px; height:10px; "></b>
</table>
</div>
<?}?>

<!-- 소개 제일 윗단--> 
<div class="container-fluid" id="intro1">

	
	<div class="row-fluid" >
		<div class="span10 offset2">
			<ul class="nav nav-pills" id="img-ul">
			<li><img class="img-polariod" src="../images/medi4.png"/>
				<h3>진단 소개</h3>
				<p>내 강아지의 상태만으로</p> 
				<p>몸상태를 진단할 수 있어요</p>
			</li>
			<li><img class="img-polariod" src="../images/person.png"/>
			    <h3>이럴때 당황마세요</h3>
				<p>내 강아지의 현상태는?</p>
				<p>상황별 대처법을 알 수 있어요</p></li>
			<li><img class="img-polariod" src="../images/board4.png"/>
				<h3>게시판 소개</h3>
				<p>애견인들과 소통하고</p>
				<p>귀여운 내 강아지를 자랑해보세요</p>
			</li>
		</ul>
		</div>
	</div>
</div>

	<!-- 두번 째 소개-->
<div class="container-fluid" id="intro2">
		<div class="row-fluid" >
			
				<div class="span7">

					<div id="img-div1">
						<img src="../images/arrow_l.png" id="aleft">
					</div>

					<div id="cont">
					<div id="wrap">
					<img class="img-rounded" src="../images/55.png" >
					<img class="img-rounded" src="../images/66.png">
					<img class="img-rounded" src="../images/11.png">
					<img class="img-rounded" src="../images/sub2.jpg">
					<img class="img-rounded" src="../images/22.png">
					<img class="img-rounded" src="../images/88.png">
					<img class="img-rounded" src="../images/1111.png">
					<img class="img-rounded" src="../images/1010.png">
				</div>
				</div>
				</div>

				<div class="span5">

					<div id="img-div2">
					<img src="../images/arrow_r.png" id="aright">
					</div>

					<div class="row-fluid" id="img-ul2-div">
					
						<div id="text-box-div">
							<h2 >이럴때 당황하지마세요.</h2>
							<h4 >상황별 대처법을 알 수 있어요.</h4>
							<h2 class="text-select">이럴때 당황하지 마세요 2.</h2>
							<h4 class="text-select">문제 상황을 선택하세요.</h4>
							<h2 class="text-select">증상선택 1.</h2>
							<h4 class="text-select">의심되는 증상들을 선택해보세요.</h4>
							<h2 class="text-select">증상선택 2.</h2>
							<h4 class="text-select">의심되는 증상들을 선택해보세요.</h4>
							<h2 class="text-select">증상선택 3.</h2>
							<h4 class="text-select">의심되는 증상들을 선택해보세요.</h4>
							<h2 class="text-select">자유 게시판</h2>
							<h4 class="text-select">자유롭게 글을 작성해보세요.</h4>
							<h2 class="text-select">증상공유 게시판</h2>
							<h4 class="text-select">자신이 알거나 겪었던 증상들을 공유해보세요.</h4>
							<h2 class="text-select">자랑하기 게시판</h2>
							<h4 class="text-select">자신의 애견을 자랑해보세요.</h4>
						</div>
						<ul class="nav nav-pills" id="img-ul2">
							<li class="select"><img class="img-polariod" src="../images/medi4.png"/></li>
							<li><img class="img-polariod" src="../images/person.png"/></li>
							<li><img class="img-polariod" src="../images/board4.png"/></li>
						</ul>
					</div>
		
			</div>
		</div>
	</div>

<!-- 마지막 표 부분-->
<div class="container-fluid" id="intro3">
	<div class="row-fluid">
		

<h1>Q. 진단은 어떻게 하나요?</h2>
<h3>A. 각 카테고리에 맞는 부위를 확인하세요!</h4>
<table id="table1">

<tr>

<td rowspan=7><font size=7>머리</font></td>

<td>눈</td>

<td rowspan=7 ><font size=7>몸</font></td>

<td>피부/피모</td>

<td rowspan=7 ><font size=7><p>행동</p><p>&</p><p>몸짓</p></font></td>

<td>호흡</td>

</tr>

<tr>
<td rowspan=2>코</td>
<td>발톱</td>
<td rowspan=2>식욕&음식물</td>
</tr>

<tr>
<td>항문/질</td>
</tr>

<tr>
<td rowspan=2>귀</td>
<td>만지는 것</td>
<td rowspan=2>목소리</td>
</tr>

<tr>
<td>체중/체온</td>
</tr>

<tr>
<td rowspan=2>입/이빨</td>
<td rowspan=2>배(위&장)</td>
<td rowspan=2>걷는 방법 & 기타 행동</td>
</tr>

</table>
</div>
	</div>

</body>
</html>